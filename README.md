# Laravel API demo
It's just a demo of API written with Laravel.

## Requirements

* docker
* docker-compose

Bash is used for one-command local deployment, 
but commands from the file can be run directly.

## How to start

1. Copy `docker-compose.override.example.yml` to `docker-compose.override.yml`
2. Edit `docker-compose.override.yml` for your preferences.
3. Run `docker-compose up -d --build`
4. Wait for services and run `./dev_deploy.sh`.

Use url http://127.0.0.1:8002/

Default http port is 8002 can be configured in `docker-compose.override.yml`.


## Testing

Run

```
docker-compose exec app php vendor/bin/phpunit
```

## API documentation

This project uses Swagger OpenAPI 3. 
Use url http://127.0.0.1:8003 to open docs. 
Port 8003 is default in `docker-compose.override.yml`.

API is published here https://app.swaggerhub.com/apis/rNix/laravel-api-demo/1.0.0
but can't be used interactively because there is no public http host with this project.

## Auth

API guarded with JWT. 
There is user with email `demo@example.com` and password `secret` created by seeds.

## License

The MIT License
