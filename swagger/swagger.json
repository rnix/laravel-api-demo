{
  "openapi": "3.0.0",
  "info": {
    "title": "Laravel API demo",
    "description": "Products and categories",
    "version": "1.0.0"
  },
  "servers": [
    {
      "url": "http://localhost:8002/api/v1/",
      "description": "Local environment with docker-compose"
    }
  ],
  "paths": {
    "/login": {
      "post": {
        "tags": [
          "login"
        ],
        "summary": "Get access token",
        "requestBody": {
          "description": "Provide email and password",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/LoginRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Access token has granted",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "access_token": {
                      "type": "string",
                      "example": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ"
                    },
                    "token_type": {
                      "type": "string",
                      "example": "bearer"
                    },
                    "expires_in": {
                      "type": "number",
                      "example": 3600
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Wrong credentials"
          }
        }
      }
    },
    "/categories": {
      "get": {
        "tags": [
          "categories"
        ],
        "summary": "Get list of categories",
        "parameters": [
          {
            "in": "query",
            "name": "page",
            "description": "Page number",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "current_page": {
                      "type": "integer",
                      "example": 1
                    },
                    "data": {
                      "type": "array",
                      "items": {
                        "$ref": "#/components/schemas/Category"
                      }
                    },
                    "first_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/categories?page=1"
                    },
                    "last_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/categories?page=4"
                    },
                    "prev_page_url": {
                      "type": "string",
                      "example": null
                    },
                    "next_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/categories?page=2"
                    },
                    "from": {
                      "type": "integer",
                      "example": 1
                    },
                    "last_page": {
                      "type": "integer",
                      "example": 4
                    },
                    "per_page": {
                      "type": "integer",
                      "example": 10
                    },
                    "to": {
                      "type": "integer",
                      "example": 10
                    },
                    "total": {
                      "type": "integer",
                      "example": 40
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          }
        }
      },
      "post": {
        "tags": [
          "categories"
        ],
        "summary": "Add a new category",
        "requestBody": {
          "description": "Category object that needs to be added to the store",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "name"
                ],
                "properties": {
                  "name": {
                    "type": "string",
                    "example": "Coffee"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Category"
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          },
          "422": {
            "description": "Invalid input",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationError"
                }
              }
            }
          }
        },
        "security": [
          {
            "auth_app": []
          }
        ]
      }
    },
    "/categories/{categoryId}": {
      "get": {
        "tags": [
          "categories"
        ],
        "summary": "Find category by ID",
        "parameters": [
          {
            "in": "path",
            "name": "categoryId",
            "description": "ID of a category",
            "required": true,
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Category"
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          },
          "422": {
            "description": "Invalid input",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationError"
                }
              }
            }
          }
        }
      },
      "put": {
        "tags": [
          "categories"
        ],
        "summary": "Update a category",
        "parameters": [
          {
            "in": "path",
            "name": "categoryId",
            "description": "ID of a category",
            "required": true,
            "schema": {
              "type": "integer"
            }
          }
        ],
        "requestBody": {
          "description": "New data of category",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "type": "object",
                "required": [
                  "name"
                ],
                "properties": {
                  "name": {
                    "type": "string",
                    "example": "Tea"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Updated",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Category"
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          },
          "422": {
            "description": "Invalid input",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationError"
                }
              }
            }
          }
        },
        "security": [
          {
            "auth_app": []
          }
        ]
      },
      "delete": {
        "tags": [
          "categories"
        ],
        "summary": "Delete a category",
        "parameters": [
          {
            "in": "path",
            "name": "categoryId",
            "description": "ID of a category",
            "required": true,
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "Successful operation"
          },
          "401": {
            "description": "Need authentication"
          },
          "422": {
            "description": "Invalid input",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationError"
                }
              }
            }
          }
        },
        "security": [
          {
            "auth_app": []
          }
        ]
      }
    },
    "/products": {
      "get": {
        "tags": [
          "products"
        ],
        "summary": "Get list of products",
        "parameters": [
          {
            "in": "query",
            "name": "page",
            "description": "Page number",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "current_page": {
                      "type": "integer",
                      "example": 1
                    },
                    "data": {
                      "type": "array",
                      "items": {
                        "$ref": "#/components/schemas/Product"
                      }
                    },
                    "first_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/products?page=1"
                    },
                    "last_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/products?page=4"
                    },
                    "prev_page_url": {
                      "type": "string",
                      "example": null
                    },
                    "next_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/products?page=2"
                    },
                    "from": {
                      "type": "integer",
                      "example": 1
                    },
                    "last_page": {
                      "type": "integer",
                      "example": 4
                    },
                    "per_page": {
                      "type": "integer",
                      "example": 10
                    },
                    "to": {
                      "type": "integer",
                      "example": 10
                    },
                    "total": {
                      "type": "integer",
                      "example": 40
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          }
        }
      },
      "post": {
        "tags": [
          "products"
        ],
        "summary": "Add a new product",
        "requestBody": {
          "description": "Product object that needs to be added to the store",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/StoreProductRequest"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Created",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Product"
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          },
          "422": {
            "description": "Invalid input",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationError"
                }
              }
            }
          }
        },
        "security": [
          {
            "auth_app": []
          }
        ]
      }
    },
    "/products/{productId}": {
      "get": {
        "tags": [
          "products"
        ],
        "summary": "Find product by ID",
        "parameters": [
          {
            "in": "path",
            "name": "productId",
            "description": "ID of a product",
            "required": true,
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Product"
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          },
          "422": {
            "description": "Invalid input",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationError"
                }
              }
            }
          }
        }
      },
      "put": {
        "tags": [
          "products"
        ],
        "summary": "Update a product",
        "parameters": [
          {
            "in": "path",
            "name": "productId",
            "description": "ID of a product",
            "required": true,
            "schema": {
              "type": "integer"
            }
          }
        ],
        "requestBody": {
          "description": "New data of product",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                  "$ref": "#/components/schemas/StoreProductRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Updated",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Product"
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          },
          "422": {
            "description": "Invalid input",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationError"
                }
              }
            }
          }
        },
        "security": [
          {
            "auth_app": []
          }
        ]
      },
      "delete": {
        "tags": [
          "products"
        ],
        "summary": "Delete a product",
        "parameters": [
          {
            "in": "path",
            "name": "productId",
            "description": "ID of a product",
            "required": true,
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "204": {
            "description": "Successful operation"
          },
          "401": {
            "description": "Need authentication"
          },
          "422": {
            "description": "Invalid input",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/ValidationError"
                }
              }
            }
          }
        },
        "security": [
          {
            "auth_app": []
          }
        ]
      }
    },
    "/categories/{categoryId}/products": {
      "get": {
        "tags": [
          "categories"
        ],
        "summary": "Get list of products in a category",
        "parameters": [
          {
            "in": "path",
            "name": "categoryId",
            "description": "ID of a category",
            "required": true,
            "schema": {
              "type": "integer"
            }
          },
          {
            "in": "query",
            "name": "page",
            "description": "Page number",
            "schema": {
              "type": "integer"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "current_page": {
                      "type": "integer",
                      "example": 1
                    },
                    "data": {
                      "type": "array",
                      "items": {
                        "$ref": "#/components/schemas/Product"
                      }
                    },
                    "first_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/categories/1/products?page=1"
                    },
                    "last_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/categories/1/products?page=4"
                    },
                    "prev_page_url": {
                      "type": "string",
                      "example": null
                    },
                    "next_page_url": {
                      "type": "string",
                      "example": "http://127.0.0.1:8002/api/v1/categories/1/products?page=2"
                    },
                    "from": {
                      "type": "integer",
                      "example": 1
                    },
                    "last_page": {
                      "type": "integer",
                      "example": 4
                    },
                    "per_page": {
                      "type": "integer",
                      "example": 10
                    },
                    "to": {
                      "type": "integer",
                      "example": 10
                    },
                    "total": {
                      "type": "integer",
                      "example": 40
                    }
                  }
                }
              }
            }
          },
          "401": {
            "description": "Need authentication"
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Category": {
        "type": "object",
        "required": [
          "name"
        ],
        "properties": {
          "id": {
            "type": "integer",
            "example": 123
          },
          "name": {
            "type": "string",
            "example": "Tools"
          }
        }
      },
      "Product": {
        "type": "object",
        "required": [
          "name"
        ],
        "properties": {
          "id": {
            "type": "integer",
            "example": 123
          },
          "name": {
            "type": "string",
            "example": "Screwdriver"
          },
          "description": {
            "type": "string",
            "example": "A perfect screwdriver made in Germany"
          },
          "categories": {
            "type": "array",
            "items": {
              "$ref": "#/components/schemas/Category"
            }
          }
        }
      },
      "StoreProductRequest": {
        "type": "object",
        "required": [
          "name",
          "categories"
        ],
        "properties": {
          "name": {
            "type": "string",
            "example": "Tea"
          },
          "categories": {
            "type": "array",
            "description": "List of categories ID",
            "example": [1, 25, 37],
            "items": {
              "type": "integer"
            }
          },
          "description": {
            "type": "string",
            "example": "A ceylon black tea"
          }
        }
      },
      "ValidationError": {
        "type": "object",
        "properties": {
          "message": {
            "type": "string",
            "example": "The given data was invalid."
          },
          "errors": {
            "type": "object",
            "example": {
              "name": [
                "The name field is required."
              ]
            }
          }
        }
      },
      "LoginRequest": {
        "type": "object",
        "required": [
          "email",
          "password"
        ],
        "properties": {
          "email": {
            "type": "string",
            "example": "demo@example.com"
          },
          "password": {
            "type": "string",
            "example": "secret"
          }
        }
      }
    },
    "securitySchemes": {
      "auth_app": {
        "type": "http",
        "scheme": "bearer",
        "bearerFormat": "JWT"
      }
    }
  }
}
