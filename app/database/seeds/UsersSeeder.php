<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Runs the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 10)->create();

        try {
            factory(App\Models\User::class)->create([
                'email' => 'demo@example.com',
            ]);
        } catch (\Throwable $exception) {
            // Demo user already exists, nothing to do
        }
    }
}
