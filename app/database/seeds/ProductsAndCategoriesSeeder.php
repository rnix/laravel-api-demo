<?php

use Illuminate\Database\Seeder;

class ProductsAndCategoriesSeeder extends Seeder
{
    /**
     * Runs the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Category::class, 10)->create();

        factory(\App\Models\Product::class, 50)->create()->each(function (\App\Models\Product $product) {
            $product->categories()->sync(
                \App\Models\Category::all()->random(3)
            );
        });
    }
}
