<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    public function testLogin()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $data = [
            'email' => $user->email,
            'password' => 'secret',
        ];
        $this->json('post', '/api/v1/login', $data)
            ->assertOk()
            ->assertJsonStructure(
            [
                'access_token',
                'token_type',
                'expires_in',
            ]
        );
    }

    public function testLoginFail()
    {
        $data = [
            'email' => 'non-existed@emailcom',
            'password' => 'qwerty',
        ];
        $this->json('post', '/api/v1/login', $data)
            ->assertStatus(401);
    }
}
