<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CategoriesTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex()
    {
        factory(Category::class, 2)->create();

        $this->json('get', '/api/v1/categories/')
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'data' => [
                        [
                            'id',
                            'name'
                        ],
                    ]
                ]
            );
    }

    public function testGet()
    {
        /** @var Category $category */
        $category = factory(Category::class)->create();

        $this->json('get', '/api/v1/categories/' . $category->id, [], [])
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'id' => $category->id,
                        'name' => $category->name,
                    ]
                ]
            );
    }

    public function testGetNotFound()
    {
        $this->json('get', '/api/v1/categories/999')
            ->assertStatus(404);
    }

    public function testGetProducts()
    {
        /** @var Category $category */
        $category = factory(Category::class)->create();

        factory(Product::class, 3)->create()->each(function (\App\Models\Product $product) use ($category) {
            $product->categories()->attach($category->id);
        });

        $response = $this->json('get', '/api/v1/categories/' . $category->id . '/products')
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'data' => [
                        [
                            'id',
                            'name'
                        ],
                    ]
                ]
            );

        $products = $response->json('data');
        $this->assertEquals(3, count($products));
    }

    public function testCreate()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        $data = [
            'name' => 'Test name',
        ];

        $this->json('post', '/api/v1/categories', $data, $headers)
            ->assertStatus(201)
            ->assertJsonStructure(
                ['data' => ['id', 'name', ]]
            );

        auth()->logout();
        $this->json('post', '/api/v1/categories', $data, [])
            ->assertStatus(401);
    }

    public function testCreateInvalidData()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        $data = [];

        $this->json('post', '/api/v1/categories', $data, $headers)
            ->assertStatus(422);
    }

    public function testUpdate()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        /** @var Category $category */
        $category = factory(Category::class)->create();

        $data = [
            'name' => 'Test name',
        ];

        $this->json('put', '/api/v1/categories/' . $category->id, $data, $headers)
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'id' => $category->id,
                        'name' => 'Test name',
                    ]
                ]
            );

        auth()->logout();
        $this->json('put', '/api/v1/categories/' . $category->id, $data, [])
            ->assertStatus(401);
    }

    public function testUpdateInvalidData()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        /** @var Category $category */
        $category = factory(Category::class)->create();

        $this->json('put', '/api/v1/categories/' . $category->id, [], $headers)
            ->assertStatus(422);
    }

    public function testDelete()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        /** @var Category $category */
        $category = factory(Category::class)->create();

        $data = [];

        $this->json('delete', '/api/v1/categories/' . $category->id, $data, $headers)
            ->assertStatus(204);

        $this->assertDatabaseMissing('categories', [
            'id' => $category->id
        ]);

        auth()->logout();
        $this->json('delete', '/api/v1/categories/' . $category->id, $data, [])
            ->assertStatus(401);
    }

}
