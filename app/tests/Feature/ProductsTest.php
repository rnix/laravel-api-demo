<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex()
    {
        factory(Product::class, 2)->create();

        $this->json('get', '/api/v1/products/')
            ->assertStatus(200)
            ->assertJsonStructure(
                [
                    'data' => [
                        [
                            'id',
                            'name',
                            'description'
                        ],
                    ]
                ]
            );
    }

    public function testGet()
    {
        /** @var Product $product */
        $product = factory(Product::class)->create();

        $this->json('get', '/api/v1/products/' . $product->id, [], [])
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'id' => $product->id,
                        'name' => $product->name,
                        'description' => $product->description,
                    ]
                ]
            );
    }

    public function testGetNotFound()
    {
        $this->json('get', '/api/v1/products/999')
            ->assertStatus(404);
    }

    public function testCreate()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        /** @var Category $category */
        $category = factory(Category::class)->create();

        $data = [
            'name' => 'Test name',
            'categories' => [
                $category->id,
            ],
            'description' => 'Test description',
        ];

        $this->json('post', '/api/v1/products', $data, $headers)
            ->assertStatus(201)
            ->assertJson(
                [
                    'data' => [
                        'name' => 'Test name',
                        'description' => 'Test description',
                        'categories' => [[
                            'id' => $category->id,
                            'name' => $category->name,
                        ]],
                    ],
                ]
            );

        auth()->logout();
        $this->json('post', '/api/v1/products', $data, [])
            ->assertStatus(401);
    }

    public function testCreateInvalidData()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        $data = [];

        $this->json('post', '/api/v1/products', $data, $headers)
            ->assertStatus(422);
    }

    public function testUpdate()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        /** @var Product $product */
        $product = factory(Product::class)->create();

        /** @var Category $category */
        $category = factory(Category::class)->create();

        $data = [
            'name' => 'Test name',
            'categories' => [
                $category->id,
            ],
            'description' => 'Test description',
        ];

        $this->json('put', '/api/v1/products/' . $product->id, $data, $headers)
            ->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'name' => 'Test name',
                        'description' => 'Test description',
                        'categories' => [[
                            'id' => $category->id,
                            'name' => $category->name,
                        ]],
                    ],
                ]
            );

        auth()->logout();
        $this->json('put', '/api/v1/products/' . $product->id, $data, [])
            ->assertStatus(401);
    }

    public function testUpdateInvalidData()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        /** @var Product $product */
        $product = factory(Product::class)->create();

        $data = [
            'name' => 'Test name',
            'categories' => [
                999,
            ],
            'description' => 'Test description',
        ];

        $this->json('put', '/api/v1/products/' . $product->id, $data, $headers)
            ->assertStatus(422);
    }

    public function testDelete()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = auth()->login($user);
        $headers = ['Authorization' => "Bearer $token"];

        /** @var Product $product */
        $product = factory(Product::class)->create();

        $data = [];

        $this->json('delete', '/api/v1/products/' . $product->id, $data, $headers)
            ->assertStatus(204);

        $this->assertDatabaseMissing('products', [
            'id' => $product->id
        ]);

        auth()->logout();
        $this->json('delete', '/api/v1/products/' . $product->id, $data, [])
            ->assertStatus(401);
    }

}
