<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProduct;
use App\Http\Resources\ProductResource;
use App\Models\Product;

class ProductController extends Controller
{
    protected const ELEMENTS_PER_PAGE = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ProductResource::collection(Product::paginate(self::ELEMENTS_PER_PAGE));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProduct $request
     * @return ProductResource
     */
    public function store(StoreProduct $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;

        $product->save();
        $product->categories()->sync($request->categories);

        return new ProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return ProductResource
     */
    public function show(Product $product)
    {
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreProduct $request
     * @param Product $product
     * @return ProductResource
     */
    public function update(StoreProduct $request, Product $product)
    {
        $product->name = $request->name;
        $product->description = $request->description;

        $product->save();
        $product->categories()->sync($request->categories);

        return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return response()->json(null, 204);
    }
}
