<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCategory;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Models\Category;

class CategoryController extends Controller
{
    protected const ELEMENTS_PER_PAGE = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return CategoryResource::collection(Category::paginate(self::ELEMENTS_PER_PAGE));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCategory $request
     * @return CategoryResource
     */
    public function store(StoreCategory $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->save();

        return new CategoryResource($category);
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return CategoryResource
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreCategory $request
     * @param Category $category
     * @return CategoryResource
     */
    public function update(StoreCategory $request, Category $category)
    {
        $category->name = $request->name;
        $category->save();

        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Category $category)
    {
        $category->delete();

        return response()->json(null, 204);
    }

    /**
     * Returns list of products in current category
     *
     * @param Category $category
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getProducts(Category $category)
    {
        return ProductResource::collection($category->products()->paginate(self::ELEMENTS_PER_PAGE));
    }
}
