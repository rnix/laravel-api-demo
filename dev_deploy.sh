#!/usr/bin/env bash

set -e -o xtrace

docker-compose exec app cp .env.example .env
docker-compose exec app composer install
docker-compose exec app php artisan key:generate --ansi
docker-compose exec app bash -c "id -u > user_id.txt"
docker-compose exec --user=root app bash -c 'read -r USER_ID<user_id.txt && chgrp -R ${USER_ID} storage bootstrap/cache && echo ${USER_ID}'
docker-compose exec --user=root app chmod -R ug+rwx storage bootstrap/cache
docker-compose exec app php artisan migrate --seed
docker-compose exec app php artisan jwt:secret
